package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	private static List<Country> countries = new ArrayList<>();
	
	
	static {
		countries.add(new Country("1", "India"));
		countries.add(new Country("2","Singapore"));
	}
	@GetMapping("/countries")
	public List<Country> getCountries() {
		return countries;
	}
	
	@GetMapping("/country/{id}")
	public Country getCountry(@PathVariable("id") String id) {
		return "1".equals(id)? countries.get(0) : "2".equals(id) ? countries.get(1) : null;
	}

	
}

class Country {
	
	
	private String id;
	private String name;

	 Country() {
		this.id = "";
		this.name = "";
	}

	Country(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
